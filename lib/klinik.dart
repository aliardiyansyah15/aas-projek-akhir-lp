import 'package:flutter/material.dart';
import 'package:flutter_application_pisikologi/PilihTanggalLokasi.dart';

void main() {
  runApp(MaterialApp(
    title: 'Kunjungan Klinik',
    home: ClinicVisit(),
    color: Color.fromRGBO(123, 64, 233, 1),
  ));
}

class ClinicVisit extends StatefulWidget {
  @override
  _ClinicVisitState createState() => _ClinicVisitState();
}

class _ClinicVisitState extends State<ClinicVisit> {
  final List<Map<String, dynamic>> doctors = [
    {
      'doctor': 'Dr. Lisa, S.Psi., M.Psi Psikolog',
      'location': 'Indonesian Psychological Healthcare Center',
      'doctorId': 'Dr. Lisa, S.Psi.M.Si. Psikolog',
      'image': 'images/doctor1.jpg',
      'rating': 4.5,
      'price': 500000,
    },
    {
      'doctor': 'Dr. Jhon, S.Psi., M.Psi Psikolog',
      'location': 'Yayasan Praktek Psikolog Indonesia Semarang',
      'doctorId': 'Dr. Jhon, S.Psi.',
      'image': 'images/doctor2.jpg',
      'rating': 4.0,
      'price': 400000,
    },
    {
      'doctor': 'Dr. Sarah, S.Psi., M.Psi Psikolog',
      'location': 'RS Hermina Pandanaran',
      'doctorId': 'Dr. Sarah, S.Psi., M.Psi., Psikolog',
      'image': 'images/doctor3.jpg',
      'rating': 4.8,
      'price': 550000,
    },
    {
      'doctor': 'Dr. Emily, S.Psi., M.Psi Psikolog',
      'location': 'RS Hermina Banyumanik',
      'doctorId': 'Dr. Emily, M.Si. Psikolog',
      'image': 'images/doctor4.jpg',
      'rating': 4.2,
      'price': 600000,
    },
    {
      'doctor': 'Dr. Michel, S.Psi., M.Psi., Psikolog',
      'location': 'Yayasan Praktek Psikolog Indonesia Semarang',
      'doctorId': 'Dr. Michel,S.Psi., M.Psi., Psikolog',
      'image': 'images/doctor5.jpg',
      'rating': 4.7,
      'price': 100000,
    },
  ];

  List<Map<String, dynamic>> filteredDoctors = [];

  @override
  void initState() {
    super.initState();
    filteredDoctors = doctors;
  }

  void filterDoctors(String query) {
    setState(() {
      filteredDoctors = doctors.where((doctor) {
        final doctorName = doctor['doctor'].toString().toLowerCase();
        return doctorName.contains(query.toLowerCase());
      }).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Kunjungan Klinik',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors
            .transparent, // Mengatur latar belakang AppBar menjadi transparan
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(127, 125, 235, 1),
                Color.fromRGBO(55, 84, 170, 1),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 0),
            TextField(
              onChanged: filterDoctors,
              decoration: InputDecoration(
                labelText: 'Cari Dokter',
                prefixIcon: Icon(Icons.search),
              ),
            ),
            SizedBox(height: 16),
            Expanded(
              child: ListView.builder(
                itemCount: filteredDoctors.length,
                itemBuilder: (context, index) {
                  final doctor = filteredDoctors[index];
                  return ClinicVisitItem(
                    doctor: doctor['doctor'],
                    location: doctor['location'],
                    doctorId: doctor['doctorId'],
                    image: doctor['image'],
                    rating: doctor['rating'],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ClinicVisitItem extends StatelessWidget {
  final String doctor;
  final String location;
  final String doctorId;
  final String image;
  final double rating;

  ClinicVisitItem({
    required this.doctor,
    required this.location,
    required this.doctorId,
    required this.image,
    required this.rating,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 2,
            blurRadius: 4,
            offset: Offset(0, 2),
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: 120,
            height: 200,
            child: Align(
              alignment: Alignment.center,
              child: CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage(image),
              ),
            ),
          ),
          SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  doctor,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                SizedBox(width: 4),
                Row(
                  children: [
                    Icon(
                      Icons.star,
                      size: 16,
                      color: Colors.amber,
                    ),
                    SizedBox(width: 4),
                    Text(
                      rating.toString(),
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey[700],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 4),
                Text(
                  'Konsultasi Psikolog',
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey[700],
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  location,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey[700],
                  ),
                ),
                SizedBox(height: 8),
                Row(
                  children: [
                    Icon(
                      Icons.calendar_month,
                      size: 16,
                      color: Colors.grey[700],
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PilihTanggalLokasi(
                              doctorId: doctorId,
                              doctorImage: image,
                            ),
                          ),
                        );
                      },
                      child: Text(
                        'Buat Janji Konsultasi',
                        style: TextStyle(
                          fontSize: 14,
                          color: Color.fromRGBO(123, 64, 233, 1),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
