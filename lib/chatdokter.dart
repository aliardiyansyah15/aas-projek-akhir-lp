import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(ChatDokterApp());
}

class Doctor {
  final String fullName;
  final String speciality;
  final String location;
  final String image;
  final List<String> responses;

  Doctor({
    required this.fullName,
    required this.speciality,
    required this.location,
    required this.image,
    required this.responses,
  });
}

class ChatDokterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  final List<Doctor> doctors = [
    Doctor(
      fullName: "Dr. Lisa, S.Psi., M.Psi Psikolog",
      speciality: "Psikolog",
      location: "Indonesian Psychological Healthcare Center",
      image: "images/doctor1.jpg",
      responses: [
        "Halo, apa yang bisa saya bantu?",
        "Bagaimana perasaan Anda hari ini?",
        "Ada yang ingin Anda diskusikan?",
      ],
    ),
    Doctor(
      fullName: "Dr. John, S.Psi., M.Psi",
      speciality: "Psikolog",
      location: "Yayasan Praktek Psikolog Indonesia Semarang",
      image: "images/doctor2.jpg",
      responses: [
        "Hai, ada yang ingin Anda tanyakan?",
        "Bagaimana perasaan Anda setelah konsultasi sebelumnya?",
        "Saya di sini untuk membantu Anda.",
      ],
    ),
    Doctor(
      fullName: "Dr. Sarah, S.Psi., M.Psi Psikolog",
      speciality: "Psikolog",
      location: "RS Hermina Pandanaran",
      image: "images/doctor3.jpg",
      responses: [
        "Selamat datang! Apa yang ingin Anda bicarakan hari ini?",
        "Bagaimana keadaan Anda sekarang?",
        "Jangan ragu untuk bertanya kepada saya.",
      ],
    ),
    Doctor(
      fullName: "Dr. Emily, M.Psi Psikolog",
      speciality: "Psikolog",
      location: "Rumah Sakit Jiwa Artha Medika",
      image: "images/doctor4.jpg",
      responses: [
        "Halo, apa yang bisa saya bantu?",
        "Bagaimana perasaan Anda hari ini?",
        "Mari kita cari solusi bersama.",
      ],
    ),
    Doctor(
      fullName: "Dr. Michael,S.Psi., M.Psi Psikolog",
      speciality: "Psikolog",
      location: "RS Jiwa Soedjarwadi",
      image: "images/doctor5.jpg",
      responses: [
        "Hai, ada yang ingin Anda diskusikan?",
        "Bagaimana perasaan Anda setelah konsultasi sebelumnya?",
        "Jangan ragu untuk bertanya kepada saya tentang masalah Anda.",
      ],
    ),
    // Tambahkan lebih banyak dokter sesuai kebutuhan.
  ];
  // Tambahkan lebih banyak dokter sesuai kebutuhan.

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Pilih Dokter",
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(127, 125, 235, 1), // Start color
                Color.fromRGBO(55, 84, 170, 1), // End color
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: doctors.length,
        itemBuilder: (context, index) {
          return ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage(doctors[index].image),
            ),
            title: Text(
              doctors[index].fullName,
            ),
            subtitle: Text(doctors[index].speciality),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      ChatScreen(selectedDoctor: doctors[index]),
                ),
              );
            },
          );
        },
      ),
    );
  }
}

class ChatScreen extends StatefulWidget {
  final Doctor selectedDoctor;

  ChatScreen({required this.selectedDoctor});

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final TextEditingController _textController = TextEditingController();
  final List<ChatMessage> _messages = [];

  void _handleSubmitted(String text) {
    _textController.clear();
    ChatMessage message = ChatMessage(
      text: text,
      isMe: true,
    );
    setState(() {
      _messages.insert(0, message);
    });

    // Simulate doctor reply after a delay
    _simulateDoctorReply();
  }

  void _simulateDoctorReply() {
    Future.delayed(Duration(seconds: 1), () {
      String response = widget.selectedDoctor
          .responses[Random().nextInt(widget.selectedDoctor.responses.length)];
      ChatMessage message = ChatMessage(
        text: response,
        isMe: false,
      );
      setState(() {
        _messages.insert(0, message);
      });
    });
  }

  Widget _buildTextComposer() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: <Widget>[
          Flexible(
            child: TextField(
              controller: _textController,
              onSubmitted: _handleSubmitted,
              decoration: InputDecoration.collapsed(hintText: "Ketik pesan..."),
            ),
          ),
          IconButton(
            icon: Icon(Icons.send),
            onPressed: () => _handleSubmitted(_textController.text),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        title: Row(
          children: [
            CircleAvatar(
              backgroundImage: AssetImage(widget.selectedDoctor.image),
            ),
            SizedBox(width: 8),
            Text(
              widget.selectedDoctor.fullName,
              style: TextStyle(fontSize: 14),
            ),
          ],
        ),
        actions: [
          IconButton(
            onPressed: () {
              // Aksi ketika tombol panggilan telepon di tekan
              // Misalnya, munculkan dialog panggilan telepon
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Panggilan Telepon'),
                    content: Text(
                        'Anda akan melakukan panggilan telepon ke dokter.'),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Batal'),
                      ),
                      TextButton(
                        onPressed: () {
                          // Kode untuk melakukan panggilan telepon
                          // ...
                          Navigator.pop(context);
                        },
                        child: Text('Panggil'),
                      ),
                    ],
                  );
                },
              );
            },
            icon: Icon(
              Icons.phone,
              color: Colors.white, // Warna ikon panggilan telepon
            ),
          ),
          IconButton(
            icon: Icon(Icons.video_call),
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Panggilan Video'),
                    content:
                        Text('Anda akan melakukan panggilan video ke dokter.'),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Batal'),
                      ),
                      TextButton(
                        onPressed: () {
                          // Kode untuk melakukan panggilan video
                          // ...
                          Navigator.pop(context);
                        },
                        child: Text('Panggil'),
                      ),
                    ],
                  );
                },
              );
              // Add logic here to handle video call action
              // For example, you can initiate a video call with the doctor.
            },
          ),
        ],
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(127, 125, 235, 1), // Start color
                Color.fromRGBO(55, 84, 170, 1), // End color
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            child: ListView.builder(
              reverse: true,
              padding: const EdgeInsets.all(8.0),
              itemBuilder: (_, int index) => _messages[index],
              itemCount: _messages.length,
            ),
          ),
          Divider(height: 1.0),
          Container(
            decoration: BoxDecoration(color: Theme.of(context).cardColor),
            child: _buildTextComposer(),
          ),
        ],
      ),
    );
  }
}

class ChatMessage extends StatelessWidget {
  final String text;
  final bool isMe;

  ChatMessage({required this.text, required this.isMe});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        mainAxisAlignment:
            isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              decoration: BoxDecoration(
                color: isMe ? Colors.blueAccent : Colors.grey[300],
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Text(
                text,
                style: TextStyle(fontSize: 16.0),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
