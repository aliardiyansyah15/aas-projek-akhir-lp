import 'package:flutter/material.dart';

void main() {
  runApp(PsychologyQuestionsApp());
}

class PsychologyQuestionsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: PsychologyQuestionsScreen(),
    );
  }
}

class PsychologyQuestionsScreen extends StatefulWidget {
  @override
  _PsychologyQuestionsScreenState createState() =>
      _PsychologyQuestionsScreenState();
}

class _PsychologyQuestionsScreenState extends State<PsychologyQuestionsScreen> {
  final PageController _pageController = PageController();
  final List<String> questions = [
    "Bagaimana perasaan Anda hari ini?",
    "Apakah ada peristiwa yang membuat Anda merasa tertekan?",
    "Bagaimana tidur Anda belakangan ini?",
    "Apa hobi atau kegiatan yang biasa Anda nikmati?",
    "Apakah ada hal yang membuat Anda merasa bahagia akhir-akhir ini?",
    "Apakah Anda merasa cemas mengenai masa depan?",
    "Bagaimana cara Anda mengatasi stres?",
    "Apakah Anda merasa kesulitan dalam mengendalikan emosi?",
    "Apakah ada hal yang membuat Anda merasa rendah diri?",
    "Apakah Anda memiliki dukungan sosial yang mencukupi?",
  ];

  List<String> answers = List.filled(10, '');
  void showSummaryDialog() {
    String summary = "Ringkasan Jawaban:\n\n";
    for (int i = 0; i < questions.length; i++) {
      summary += "Pertanyaan ${i + 1}: ${questions[i]}\n";
      summary += "Jawaban ${i + 1}: ${answers[i]}\n\n";
    }

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Ringkasan Jawaban'),
          content: SingleChildScrollView(
            child: Text(summary),
          ),
          actions: <Widget>[
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Tutup'),
            ),
          ],
        );
      },
    );
  }

  bool areAllQuestionsAnswered() {
    // Cek apakah ada pertanyaan yang belum dijawab
    for (String answer in answers) {
      if (answer.isEmpty) {
        return false;
      }
    }
    return true;
  }

  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pertanyaan Psikologi'),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(127, 125, 235, 1),
                Color.fromRGBO(55, 84, 170, 1),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
        ),
      ),
      body: PageView.builder(
        controller: _pageController,
        itemCount: questions.length,
        itemBuilder: (context, index) {
          return _buildQuestionCard(context, index);
        },
      ),
    );
  }

  Widget _buildQuestionCard(BuildContext context, int index) {
    TextEditingController _answerController =
        TextEditingController(text: answers[index]);

    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                questions[index],
                style: TextStyle(fontSize: 18),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              TextField(
                controller: _answerController,
                maxLines: 3,
                decoration: InputDecoration(
                  hintText: 'Ketik jawaban Anda di sini...',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (index > 0)
                    ElevatedButton(
                      onPressed: () {
                        _pageController.previousPage(
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeInOut);
                      },
                      child: Text('Sebelumnya'),
                    ),
                  if (index < questions.length - 1)
                    ElevatedButton(
                      onPressed: () {
                        _pageController.nextPage(
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeInOut);
                      },
                      child: Text('Selanjutnya'),
                    ),
                  if (index == questions.length - 1)
                    ElevatedButton(
                      onPressed: () {
                        String answer = _answerController.text.trim();
                        if (answer.isEmpty) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text('Harap lengkapi semua pertanyaan'),
                            ),
                          );
                          return;
                        }
                        setState(() {
                          answers[index] =
                              answer; // Simpan jawaban ke dalam list jawaban
                        });

                        // Tampilkan dialog ringkasan jawaban setelah pengguna menjawab pertanyaan terakhir
                        showSummaryDialog();
                      },
                      child: Text('Simpan'),
                    ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
