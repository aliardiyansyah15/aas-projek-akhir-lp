import 'package:flutter/material.dart';
import 'PilihTanggalLokasi.dart'; // Pastikan Anda sudah memiliki file ini dalam proyek Anda

class riwayat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Riwayat Konsultasi Dokter',
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(127, 125, 235, 1),
                Color.fromRGBO(55, 84, 170, 1),
              ], // Ganti warna gradasi sesuai keinginan Anda
            ),
          ),
        ),
      ),
      body: ListView(
        children: [
          RiwayatItem(
            tanggal: '01 Juli 2023',
            lokasi: 'Yayasan Praktek Psikolog Indonesia Semarang',
            dokter: 'Dr. Michel,S.Psi., M.Psi., Psikolog',
            spesialisasi: 'Dokter Psikologi',
          ),
          RiwayatItem(
            tanggal: '15 Juni 2023',
            lokasi: 'Indonesian Psychological Healthcare Center',
            dokter: 'Dr. Lisa, S.Psi., M.Psi Psikolog',
            spesialisasi: 'Dokter Pssikologi',
          ),
          // Tambahkan item riwayat lainnya sesuai kebutuhan
        ],
      ),
    );
  }
}

// Widget untuk menampilkan item riwayat konsultasi dokter
class RiwayatItem extends StatelessWidget {
  final String tanggal;
  final String lokasi;
  final String dokter;
  final String spesialisasi;

  RiwayatItem({
    required this.tanggal,
    required this.lokasi,
    required this.dokter,
    required this.spesialisasi,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.history), // Ikon untuk item riwayat
      title: Text('Konsultasi pada $tanggal'),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Lokasi: $lokasi'),
          Text('Dokter: $dokter'),
          Text('Spesialisasi: $spesialisasi'),
        ],
      ),
      onTap: () {
        // Logika untuk menampilkan detail riwayat konsultasi atau navigasi ke halaman detail
      },
    );
  }
}
