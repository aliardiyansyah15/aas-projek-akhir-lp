import 'package:flutter/material.dart';

class HalamanNotifikasi extends StatelessWidget {
  const HalamanNotifikasi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 30.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text('Berita Kesehatan'),
              background: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromRGBO(127, 125, 235, 1),
                      Color.fromRGBO(55, 84, 170, 1),
                    ], // Ganti warna gradasi sesuai keinginan.
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                ),
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return BeritaKesehatanCard(
                  title: _beritaKesehatan[index]['title']!,
                  description: _beritaKesehatan[index]['description']!,
                  imageUrl: _beritaKesehatan[index]['imageUrl']!,
                );
              },
              childCount: _beritaKesehatan.length,
            ),
          ),
        ],
      ),
    );
  }
}

class BeritaKesehatanCard extends StatelessWidget {
  final String title;
  final String description;
  final String imageUrl;

  const BeritaKesehatanCard({
    required this.title,
    required this.description,
    required this.imageUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            imageUrl,
            width: double.infinity,
            height: 200,
            fit: BoxFit.cover,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 8),
                Text(description),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// Dummy data for health news (Replace this with your actual data)
final List<Map<String, String>> _beritaKesehatan = [
  {
    'title': '10 Manfaat Konsultasi Psikologi untuk Kesehatan Mental',
    'description':
        'Liputan6.com, Jakarta Manfaat konsultasi psikologi sama pentingnya dengan konsultasi kesehatan lainnya. Setiap orang bisa menghadapi tantangan kesehatan mental pada satu titik dalam hidup. Manfaat konsultasi psikologi sangat penting bagi kesehatan mental.',
    'imageUrl': 'images/iklan4.webp', // Ganti dengan path gambar yang sesuai.
  },
  {
    'title':
        'Dirut BPJS Kesehatan Beberkan Capaian Sukses Program JKN di Oxford University',
    'description':
        'Liputan6.com, Jakarta BPJS Kesehatan mampu menjaga Program Jaminan Kesehatan Nasional (JKN) tetap berjalan hingga kini. Di tengah dinamika serta kompleksitas yang terjadi, program JKN pun kerap sukses meraih beberapa capaian, salah satunya adalah jumlah kepesertaan.',
    'imageUrl': 'images/iklan5.png', // Ganti dengan path gambar yang sesuai.
  },
  {
    'title':
        '10 Unsur Kebugaran Jasmani dan Fungsinya, Dukung Kesehatan Keseluruhan',
    'description':
        'Liputan6.com, Jakarta Unsur kebugaran jasmani penting untuk dijaga. Kebugaran jasmani mengacu pada kemampuan sistem tubuh untuk bekerja sama secara efisien. Unsur kebugaran jasmani memungkinkan tubuh menjadi sehat dan bisa melakukan aktivitas sehari-hari.',
    'imageUrl': 'images/iklan6.png', // Ganti dengan path gambar yang sesuai.
  },
];
