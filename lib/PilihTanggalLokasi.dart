import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PilihTanggalLokasi extends StatefulWidget {
  final String doctorId;
  final String doctorImage;

  PilihTanggalLokasi({required this.doctorId, required this.doctorImage});

  @override
  _PilihTanggalLokasiState createState() => _PilihTanggalLokasiState();
}

class _PilihTanggalLokasiState extends State<PilihTanggalLokasi> {
  DateTime? selectedDate;
  TimeOfDay? selectedTime;
  String? selectedPaymentMethod;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Pilih Tanggal dan Waktu',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.transparent,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(127, 125, 235, 1),
                Color.fromRGBO(55, 84, 170, 1),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: 0),
              CircleAvatar(
                radius: 180,
                backgroundImage: AssetImage(widget.doctorImage),
              ),
              SizedBox(height: 8),
              Center(
                child: Text(
                  ' ${widget.doctorId}',
                  style: TextStyle(fontSize: 16),
                ),
              ),
              SizedBox(height: 24),
              Text(
                'Pilih Tanggal:',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 8),
              ElevatedButton.icon(
                onPressed: () {
                  _selectDate(context);
                },
                icon: Icon(Icons.calendar_today),
                label: Text(
                  selectedDate != null
                      ? '${DateFormat('dd/MM/yyyy').format(selectedDate!)}'
                      : 'Pilih Tanggal dan Waktu',
                ),
                style: ElevatedButton.styleFrom(
                  primary: Color.fromRGBO(55, 84, 170, 1),
                  onPrimary: Colors.white,
                ),
              ),
              SizedBox(height: 24),
              if (selectedDate != null) ...[
                Text(
                  'Pilih Waktu:',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 8),
                ElevatedButton.icon(
                  onPressed: () {
                    _selectTime(context);
                  },
                  icon: Icon(Icons.access_time),
                  label: Text(
                    selectedTime != null
                        ? ' ${selectedTime!.format(context)}'
                        : 'Pilih Waktu',
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Color.fromRGBO(55, 84, 170, 1),
                    onPrimary: Colors.white,
                  ),
                ),
                SizedBox(height: 24),
              ],
              Text(
                'Pilih Metode Pembayaran:',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 8),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Color.fromRGBO(55, 84, 170, 1),
                  ),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Row(
                  children: [
                    DropdownButton<String>(
                      value: selectedPaymentMethod,
                      elevation: 16,
                      style: TextStyle(color: Colors.black),
                      underline: SizedBox(),
                      onChanged: (String? newValue) {
                        setState(() {
                          selectedPaymentMethod = newValue;
                        });
                      },
                      items: <String>[
                        'OVO',
                        'GoPay',
                        'Dana',
                        'Transfer Bank',
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                    SizedBox(width: 8),
                    Text(
                      'Rp 500.000,00',
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 24),
              ElevatedButton(
                onPressed: () {
                  if (selectedDate != null &&
                      selectedTime != null &&
                      selectedPaymentMethod != null) {
                    _showConfirmationDialog();
                  } else {
                    Fluttertoast.showToast(
                      msg:
                          'Silakan pilih tanggal, waktu, dan metode pembayaran',
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                    );
                  }
                },
                child: Text('Buat Janji Konsultasi'),
                style: ElevatedButton.styleFrom(
                  primary: Color.fromRGBO(55, 84, 170, 1),
                  onPrimary: Colors.white,
                  textStyle: TextStyle(fontSize: 18),
                  padding: EdgeInsets.symmetric(vertical: 16),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(Duration(days: 30)),
    );

    if (pickedDate != null) {
      setState(() {
        selectedDate = pickedDate;
        selectedTime = null; // Reset selected time when a new date is picked
      });
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? pickedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (pickedTime != null) {
      setState(() {
        selectedTime = pickedTime;
      });
    }
  }

  void _showConfirmationDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Konfirmasi Janji Konsultasi'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Dokter: ${widget.doctorId}'),
              SizedBox(height: 8),
              Text(
                  'Tanggal: ${DateFormat('dd/MM/yyyy').format(selectedDate!)}'),
              SizedBox(height: 8),
              Text('Waktu: ${selectedTime!.format(context)}'),
              SizedBox(height: 8),
              Text('Metode Pembayaran: $selectedPaymentMethod'),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Batal'),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
                _showSuccessToast();
              },
              child: Text('Konfirmasi'),
              style: ElevatedButton.styleFrom(
                primary: Color.fromRGBO(123, 64, 233, 1),
                onPrimary: Colors.white,
              ),
            ),
          ],
        );
      },
    );
  }

  void _showSuccessToast() {
    Fluttertoast.showToast(
      msg: 'Janji Konsultasi Berhasil Dibuat',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.green,
      textColor: Colors.white,
    );
  }
}
