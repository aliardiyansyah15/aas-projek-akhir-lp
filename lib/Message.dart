import 'package:flutter/material.dart';

class Doctor {
  final String name;
  final String specialty;
  final String imagePath;

  Doctor({
    required this.name,
    required this.specialty,
    required this.imagePath,
  });
}

class Message extends StatefulWidget {
  final Doctor doctor;

  Message({required this.doctor});

  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> {
  final TextEditingController _textEditingController = TextEditingController();
  List<String> _messages = [];
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _messages.add("Selamat datang! Apa yang ingin Anda bicarakan hari ini?");
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void _sendMessage(String message) {
    if (message.isNotEmpty) {
      setState(() {
        _messages.add(message); // Gunakan add() untuk menambahkan pesan baru
      });
      _textEditingController.clear();
      _sendAutomatedResponse();
      _scrollToBottom();
    }
  }

  void _sendAutomatedResponse() {
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        _messages.add("Silahkan bisa tunggu dokter menjawab");
      });
      _scrollToBottom();
    });
  }

  void _scrollToBottom() {
    _scrollController.animateTo(
      0.0,
      duration: Duration(milliseconds: 300),
      curve: Curves.easeOut,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(127, 125, 235, 1),
                Color.fromRGBO(55, 84, 170, 1),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
        ), // Warna latar belakang app bar seperti WhatsApp
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Row(
          children: [
            CircleAvatar(
              backgroundImage: AssetImage(widget.doctor.imagePath),
              radius: 20.0,
            ),
            SizedBox(width: 8.0),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.doctor.name,
                  style: TextStyle(
                      fontSize: 13.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
                Text(
                  widget.doctor.specialty,
                  style: TextStyle(fontSize: 12.0, color: Colors.white),
                ),
              ],
            ),
          ],
        ),
        actions: [
          IconButton(
            onPressed: () {
              // Aksi ketika tombol panggilan telepon di tekan
              // Misalnya, munculkan dialog panggilan telepon
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Panggilan Telepon'),
                    content: Text(
                        'Anda akan melakukan panggilan telepon ke dokter.'),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Batal'),
                      ),
                      TextButton(
                        onPressed: () {
                          // Kode untuk melakukan panggilan telepon
                          // ...
                          Navigator.pop(context);
                        },
                        child: Text('Panggil'),
                      ),
                    ],
                  );
                },
              );
            },
            icon: Icon(
              Icons.phone,
              color: Colors.white, // Warna ikon panggilan telepon
            ),
          ),
          IconButton(
            onPressed: () {
              // Aksi ketika tombol panggilan video di tekan
              // Misalnya, munculkan dialog panggilan video
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Panggilan Video'),
                    content:
                        Text('Anda akan melakukan panggilan video ke dokter.'),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Batal'),
                      ),
                      TextButton(
                        onPressed: () {
                          // Kode untuk melakukan panggilan video
                          // ...
                          Navigator.pop(context);
                        },
                        child: Text('Panggil'),
                      ),
                    ],
                  );
                },
              );
            },
            icon: Icon(
              Icons.video_call,
              color: Colors.white, // Warna ikon panggilan video
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.separated(
              // Gunakan ListView.separated untuk menambahkan pemisah antara pesan-pesan
              separatorBuilder: (context, index) => Divider(
                color: Colors.transparent,
                height: 4.0,
              ),
              reverse: true,
              itemCount: _messages.length,
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                final message = _messages[_messages.length -
                    1 -
                    index]; // Ubah urutan pesan untuk ditampilkan terbalik
                final isDoctorMessage = index % 2 == 0;
                return Align(
                  alignment: isDoctorMessage
                      ? Alignment.centerLeft
                      : Alignment.centerRight,
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      color: isDoctorMessage
                          ? Colors.grey[300]
                          : Colors.green[200],
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Text(
                      message,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                );
              },
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(color: Colors.grey[300]!),
              ),
            ),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _textEditingController,
                    decoration: InputDecoration(
                      hintText: 'Ketik pesan Anda...',
                      border: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(width: 8.0),
                IconButton(
                  onPressed: () {
                    _sendMessage(_textEditingController.text);
                  },
                  icon: Icon(
                    Icons.send,
                    color: Colors
                        .green[900], // Warna ikon kirim pesan seperti WhatsApp
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
