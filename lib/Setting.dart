import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_pisikologi/login.dart';
import 'package:flutter_application_pisikologi/riwayat.dart';

class SettingsPage extends StatelessWidget {
  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Pengaturan',
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(127, 125, 235, 1),
                Color.fromRGBO(55, 84, 170, 1),
              ], // Ganti warna gradasi sesuai keinginan Anda
            ),
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: 6, // Jumlah opsi pengaturan
        itemBuilder: (context, index) {
          return ListTile(
            leading: _getIcon(index),
            title: _getTitle(index),
            subtitle: _getSubtitle(index),
            onTap: () {
              _handleTap(index, context);
            },
          );
        },
      ),
    );
  }

  Icon _getIcon(int index) {
    switch (index) {
      case 0:
        return Icon(Icons.info);
      case 1:
        return Icon(Icons.description);
      case 2:
        return Icon(Icons.phone);
      case 3:
        return Icon(Icons.history);
      case 4:
        return Icon(Icons.privacy_tip);
      case 5:
        return Icon(Icons.logout_outlined);
      default:
        return Icon(Icons.settings);
    }
  }

  Text _getTitle(int index) {
    switch (index) {
      case 0:
        return Text('Tentang Kami');
      case 1:
        return Text('Syarat dan Ketentuan');
      case 2:
        return Text('Hubungi Kami');
      case 3:
        return Text('Riwayat');
      case 4:
        return Text('Privasi');
      case 5:
        return Text('Logout',
            style: TextStyle(
                color: Colors.red)); // Warna teks Logout menjadi merah
      default:
        return Text('Pengaturan');
    }
  }

  Text _getSubtitle(int index) {
    switch (index) {
      case 0:
        return Text('Informasi tentang aplikasi');
      case 1:
        return Text('Persyaratan penggunaan aplikasi');
      case 2:
        return Text('Kontak dan dukungan');
      case 3:
        return Text('Riwayat konsultasi');
      case 4:
        return Text('Pengaturan privasi akun');
      default:
        return Text('');
    }
  }

  void _handleTap(int index, BuildContext context) {
    switch (index) {
      case 0:
        // Logika untuk menavigasi ke halaman "Tentang Kami"
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => TentangKamiPage(),
          ),
        );
        break;
      case 1:
        // Logika untuk menavigasi ke halaman "Syarat dan Ketentuan"
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SyaratKetentuanPage(),
          ),
        );
        break;
      case 2:
        // Logika untuk menavigasi ke halaman "Hubungi Kami"
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => HubungiKamiPage(),
          ),
        );
        break;
      case 3:
        // Logika untuk menavigasi ke halaman "Riwayat"
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => riwayat(),
          ),
        );
        break;
      case 4:
        // Logika untuk menavigasi ke halaman "Privasi"
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => PrivasiPage(),
          ),
        );
        break;
      case 5:
        signOut().then((_) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => LoginPage()),
            (Route<dynamic> route) => false,
          );
        });
        break;
      default:
        break;
    }
  }
}

// Buatlah halaman baru untuk setiap opsi yang ingin ditampilkan
class TentangKamiPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Tentang Kami',
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(127, 125, 235, 1),
                Color.fromRGBO(55, 84, 170, 1),
              ], // Ganti warna gradasi sesuai keinginan Anda
            ),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Selamat datang di Aplikasi Konsultasi Psikologi!',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 16),
            Text(
              'Tentang Kami',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 8),
            Text(
              'Aplikasi Konsultasi Psikologi adalah platform untuk membantu Anda mendapatkan dukungan dan panduan dari para profesional di bidang psikologi. Tujuan kami adalah untuk memberikan layanan yang dapat meningkatkan kesejahteraan mental dan membantu Anda mengatasi berbagai tantangan emosional dan mental yang mungkin Anda hadapi.',
            ),
            SizedBox(height: 16),
            Text(
              'Visi Kami',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 8),
            Text(
              'Menjadi platform terdepan dalam menyediakan konsultasi psikologi online yang berkualitas dan terpercaya, sehingga dapat membantu masyarakat dalam mencapai kesehatan mental yang optimal.',
            ),
            SizedBox(height: 16),
            Text(
              'Misi Kami',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 8),
            Text(
              '1. Menyediakan akses mudah dan cepat untuk konsultasi dengan para profesional psikologi.\n\n2. Menjaga kerahasiaan dan privasi informasi pengguna.\n\n3. Menyediakan lingkungan yang aman dan mendukung untuk berbicara tentang masalah pribadi dan emosional.\n\n4. Meningkatkan kesadaran masyarakat tentang pentingnya kesehatan mental dan pentingnya mencari bantuan profesional ketika diperlukan.\n\n5. Terus meningkatkan dan mengembangkan layanan kami sesuai dengan kebutuhan dan umpan balik dari pengguna.',
            ),
          ],
        ),
      ),
    );
  }
}

class SyaratKetentuanPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Syarat dan Ketentuan',
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(127, 125, 235, 1),
                Color.fromRGBO(55, 84, 170, 1),
              ], // Ganti warna gradasi sesuai keinginan Anda
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Syarat dan Ketentuan Penggunaan',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 16),
              Text(
                '1. Penggunaan Aplikasi',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                'Aplikasi Konsultasi Psikologi hanya boleh digunakan oleh individu yang berusia 18 tahun ke atas. Pengguna diwajibkan untuk memberikan informasi yang akurat dan terbaru saat mendaftar dan menggunakan layanan aplikasi.',
              ),
              SizedBox(height: 16),
              Text(
                '2. Privasi Pengguna',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                'Kami menghargai privasi Anda. Informasi pribadi yang Anda berikan saat menggunakan aplikasi akan dijaga kerahasiaannya sesuai dengan kebijakan privasi yang berlaku di Aplikasi Konsultasi Psikologi.',
              ),
              SizedBox(height: 16),
              Text(
                '3. Tanggung Jawab Pengguna',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                'Pengguna bertanggung jawab atas setiap tindakan atau informasi yang disampaikan dalam sesi konsultasi dengan profesional psikologi. Aplikasi Konsultasi Psikologi tidak bertanggung jawab atas dampak dari tindakan pengguna.',
              ),
              SizedBox(height: 16),
              Text(
                '4. Layanan Konsultasi',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                'Layanan konsultasi yang disediakan oleh Aplikasi Konsultasi Psikologi hanya bersifat informatif dan tidak menggantikan konsultasi medis atau terapi langsung. Jika Anda mengalami kondisi medis yang serius, segera hubungi tenaga medis yang berkualifikasi.',
              ),
              SizedBox(height: 16),
              Text(
                '5. Perubahan Syarat dan Ketentuan',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                'Syarat dan ketentuan penggunaan aplikasi dapat berubah dari waktu ke waktu. Perubahan tersebut akan diumumkan melalui pembaruan di aplikasi atau melalui media komunikasi lainnya. Pengguna diharapkan untuk secara rutin memeriksa syarat dan ketentuan yang berlaku.',
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class HubungiKamiPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Hubungi Kami',
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(127, 125, 235, 1),
                Color.fromRGBO(55, 84, 170, 1),
              ], // Ganti warna gradasi sesuai keinginan Anda
            ),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Hubungi Tim Konsultasi Psikologi',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 16),
            Text(
              'Jika Anda memiliki pertanyaan, masukan, atau kendala terkait penggunaan aplikasi, silakan hubungi kami melalui salah satu metode berikut:',
            ),
            SizedBox(height: 16),
            _buildContactItem(
              icon: Icons.email,
              label: 'Email',
              contactInfo: 'kontak@konsultasi-psikologi.com',
            ),
            SizedBox(height: 8),
            _buildContactItem(
              icon: Icons.phone,
              label: 'Telepon',
              contactInfo: '+62 123 4567 890',
            ),
            SizedBox(height: 8),
            _buildContactItem(
              icon: Icons.location_on,
              label: 'Alamat',
              contactInfo: 'Jalan Konsultasi Psikologi No. 2',
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildContactItem({
    required IconData icon,
    required String label,
    required String contactInfo,
  }) {
    return Row(
      children: [
        Icon(icon),
        SizedBox(width: 8),
        Text(
          '$label : $contactInfo',
          style: TextStyle(fontSize: 16),
        ),
      ],
    );
  }
}

class PrivasiPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Privasi',
          style: TextStyle(color: Colors.white),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromRGBO(127, 125, 235, 1),
                Color.fromRGBO(55, 84, 170, 1),
              ], // Ganti warna gradasi sesuai keinginan Anda
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Kebijakan Privasi Aplikasi Konsultasi Psikologi',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 16),
              Text(
                'Kami menghargai privasi Anda dan berkomitmen untuk melindungi informasi pribadi yang Anda berikan saat menggunakan aplikasi konsultasi psikologi. Dalam Kebijakan Privasi ini, kami menjelaskan jenis informasi yang kami kumpulkan, bagaimana kami menggunakannya, dan langkah-langkah yang kami ambil untuk melindungi informasi tersebut.',
              ),
              SizedBox(height: 16),
              Text(
                'Informasi yang Kami Kumpulkan',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                'Ketika Anda menggunakan aplikasi konsultasi psikologi, kami dapat mengumpulkan informasi pribadi berikut:',
              ),
              SizedBox(height: 8),
              _buildBulletPoint(
                  'Informasi akun: Nama, alamat email, foto profil, dll.'),
              _buildBulletPoint(
                  'Informasi konsultasi: Riwayat konsultasi, topik, dan informasi terkait.'),
              _buildBulletPoint(
                  'Informasi perangkat: Jenis perangkat, sistem operasi, dll.'),
              SizedBox(height: 16),
              Text(
                'Bagaimana Kami Menggunakan Informasi Anda',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                'Kami menggunakan informasi pribadi yang kami kumpulkan untuk:',
              ),
              SizedBox(height: 8),
              _buildBulletPoint('Menyediakan layanan konsultasi psikologi.'),
              _buildBulletPoint('Memproses pembayaran jika diperlukan.'),
              _buildBulletPoint(
                  'Mengirim pembaruan dan informasi terkait layanan.'),
              _buildBulletPoint('Meningkatkan kualitas dan keamanan aplikasi.'),
              SizedBox(height: 16),
              Text(
                'Bagaimana Kami Melindungi Informasi Anda',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                'Kami mengambil langkah-langkah keamanan teknis dan organisasi untuk melindungi informasi pribadi Anda, termasuk enkripsi data, akses terbatas, dan kontrol keamanan lainnya.',
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBulletPoint(String text) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(width: 8),
        Icon(Icons.fiber_manual_record, size: 12),
        SizedBox(width: 8),
        Expanded(child: Text(text)),
      ],
    );
  }
}
