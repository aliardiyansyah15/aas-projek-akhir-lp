import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_pisikologi/HalamanNotifikasi.dart';
import 'package:flutter_application_pisikologi/HalamanPesan.dart';
import 'package:flutter_application_pisikologi/Message.dart';
import 'package:flutter_application_pisikologi/klinik.dart';
import 'package:flutter_application_pisikologi/pertanyaan.dart';
import 'package:url_launcher/url_launcher.dart';

class Home extends StatelessWidget {
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    User? user = FirebaseAuth.instance.currentUser;
    String username = '';

    if (user != null) {
      if (user.displayName != null && user.displayName!.isNotEmpty) {
        username = user.displayName!;
      } else {
        username = user.email!.split('@')[0];
      }
    }

    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(top: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 5,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromRGBO(127, 125, 235, 1),
                      Color.fromRGBO(55, 84, 170, 1),
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.all(16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        // Handle profile icon tap
                      },
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 30,
                        child: Icon(
                          Icons.person,
                          color: Colors.black,
                          size: 40,
                        ),
                      ),
                    ),
                    SizedBox(width: 16),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Selamat datang,',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(height: 4),
                          Text(
                            username,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HalamanPesan()),
                            );
                          },
                          child: Icon(
                            Icons.email,
                            color: Colors.white,
                            size: 30,
                          ),
                        ),
                        SizedBox(width: 16),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HalamanNotifikasi()),
                            );
                          },
                          child: Icon(
                            Icons.notifications,
                            color: Colors.white,
                            size: 30,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 10),
            Row(
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ClinicVisit()),
                      );
                    },
                    child: Container(
                      height: 180,
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 5,
                            offset: Offset(0, 3),
                          ),
                        ],
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(127, 125, 235, 1),
                            Color.fromRGBO(55, 84, 170, 1),
                          ],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                      ),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Positioned(
                            top: 8,
                            left: 8,
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 30,
                              child: Icon(
                                Icons.add,
                                size: 40,
                                color: Color.fromRGBO(55, 84, 170, 1),
                              ),
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 7,
                                ),
                                child: Text(
                                  'Janji Dokter',
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8, bottom: 30),
                                child: Text(
                                  'Konsultasi dengan dokter Langsung',
                                  style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PsychologyQuestionsApp()),
                      );
                    },
                    child: Container(
                      height: 180,
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 5,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Positioned(
                            top: 8,
                            left: 8,
                            child: CircleAvatar(
                              backgroundColor:
                                  Color.fromARGB(255, 230, 230, 230),
                              radius: 30,
                              child: Icon(
                                Icons.question_answer_sharp,
                                size: 40,
                                color: Color.fromRGBO(55, 84, 170, 1),
                              ),
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 7,
                                ),
                                child: Text(
                                  'Konseling',
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8, bottom: 30),
                                child: Text(
                                  'Bimbingan Pertanyaan Psikologi',
                                  style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Row(
                children: [
                  Text(
                    'Apa yang anda rasakan saat ini?',
                    style: TextStyle(
                      color: Color.fromARGB(255, 190, 190, 190),
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 5),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  SizedBox(width: 5),
                  SymptomCard(
                    text: 'Ketakutan atau kegelisahan',
                    color: Color.fromRGBO(55, 84, 170, 1),
                  ),
                  SizedBox(width: 5),
                  SymptomCard(
                    text: 'Perubahan suasana hati',
                    color: Color.fromRGBO(55, 84, 170, 1),
                  ),
                  SizedBox(width: 5),
                  SymptomCard(
                    text: 'Masalah berpikir',
                    color: Color.fromRGBO(55, 84, 170, 1),
                  ),
                  SizedBox(width: 5),
                  SymptomCard(
                    text: 'Perubahan tidur atau nafsu makan',
                    color: Color.fromRGBO(55, 84, 170, 1),
                  ),
                  SizedBox(width: 5),
                  SymptomCard(
                    text: 'Penarikan diri',
                    color: Color.fromRGBO(55, 84, 170, 1),
                  ),
                  SizedBox(width: 5),
                ],
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Row(
                children: [
                  Text(
                    'Rekomendasi Dokter',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  SizedBox(width: 10),
                  DoctorCard(
                    name: 'Dr. Lisa, S.Psi., M.Psi Psikolog',
                    specialty: 'Cardiologist',
                    imagePath: 'images/doctor1.jpg',
                  ),
                  SizedBox(width: 10),
                  DoctorCard(
                    name: 'Dr. John, S.Psi., M.Psi Psikolog',
                    specialty: 'Pediatrician',
                    imagePath: 'images/doctor2.jpg',
                  ),
                  SizedBox(width: 10),
                  DoctorCard(
                    name: 'Dr. Sarah, S.Psi., M.Psi Psikolog',
                    specialty: 'Dermatologist',
                    imagePath: 'images/doctor3.jpg',
                  ),
                  SizedBox(width: 10),
                  SizedBox(width: 10),
                  DoctorCard(
                    name: 'Dr. Emily,S.Psi. M.Psi Psikolog',
                    specialty: 'Psychiatrist',
                    imagePath: 'images/doctor4.jpg',
                  ),
                  SizedBox(width: 10),
                  DoctorCard(
                    name: 'Dr. Michael, S.Psi., M.Psi Psikolog',
                    specialty: 'Orthopedic Surgeon',
                    imagePath: 'images/doctor5.jpg',
                  ),
                ],
              ),
            ),
            SizedBox(height: 100), // Adjust the height according to your needs
          ],
        ),
      ),
    );
  }
}

class SymptomCard extends StatefulWidget {
  final String text;
  final Color color;

  SymptomCard({required this.text, required this.color});

  @override
  _SymptomCardState createState() => _SymptomCardState();
}

class _SymptomCardState extends State<SymptomCard> {
  bool isTapped = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isTapped = !isTapped;
        });
      },
      child: Container(
        width: 140,
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          gradient: LinearGradient(
            colors: [
              isTapped
                  ? widget.color.withOpacity(0.6)
                  : widget.color.withOpacity(0.8),
              widget.color.withOpacity(1.0),
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          boxShadow: [
            BoxShadow(
              color: widget.color.withOpacity(0.3),
              offset: Offset(0, 3),
              blurRadius: 6,
              spreadRadius: 1,
            ),
          ],
        ),
        child: Stack(
          children: [
            Visibility(
              visible: isTapped,
              child: Positioned(
                top: 8,
                right: 8,
                child: Icon(
                  Icons.check_circle,
                  color: Colors.green,
                  size: 20,
                ),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  widget.text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DoctorCard extends StatelessWidget {
  final String name;
  final String specialty;
  final String imagePath;

  DoctorCard(
      {required this.name, required this.specialty, required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 180,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8),
              topRight: Radius.circular(8),
            ),
            child: Image.asset(
              imagePath,
              width: double.infinity,
              height: 200,
              fit: BoxFit.cover,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  specialty,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: Icon(Icons.chat),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Message(
                                  doctor: Doctor(
                                      name: name,
                                      specialty: specialty,
                                      imagePath: imagePath))),
                        );
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.phone),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text('Telepon'),
                              content: Text('Menelepon $name'),
                              actions: [
                                TextButton(
                                  child: Text('Batal'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                TextButton(
                                  child: Text('Telepon'),
                                  onPressed: () {},
                                ),
                              ],
                            );
                          },
                        );
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.video_call),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text('Video Call'),
                              content: Text('Memulai video call dengan $name'),
                              actions: [
                                TextButton(
                                  child: Text('Batal'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                TextButton(
                                  child: Text('Video Call'),
                                  onPressed: () {
                                    // Handle video call action here
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
